# El cas: Selfies a Hollywood

## El cas

- Treballes per al MrWolf de Hollywood. Un especialista en netejar la brossa de les estrelles del cine.
- A mig matí ha aparegut amb la seva habitual cara de mala llet i un maletí tacat de sang.
- La intriga per saber que hi ha al maletí s'ha acabat ràpidament. L'ha deixat sobre la teva taula i t'ha deixat anar un:
	- \"Obre el maletí. Extreu el disc. Assegura la cadena de custodia i troba les fotos\".
	- \"Quines fotos? Has dit.\"
	- \"T'ho he d'explicar tot?\" - ha bramat-. \"No diguis el nom. Manté la confidencialitat. Si algú s'entera d'això estas acabat\".

- Després t'ha explicat que un sinistre personatge ha estat espiant a una actriu i li ha robat 13 selfies una mica passats de volta. MrWolf ha recuperat el portàtil del personatge però no sap trobar les fotos. La teva feina serà recuperar-les.
- No fa falta que trobis evidencies. D'això ja s'encarregarà en MrWolf amb altres mètodes. El que es necessita són les fotos.
- La carrera de \"No es pot dir el nom\" està en joc. La teva també.

## Tasques

1. Importa el fitxer dd com una partició i utilitzant symlink.
2. Carrega tot a l'Autopsy
3. Troba les 13 fotografies i realitza un informe forense de les passes realitzades.
