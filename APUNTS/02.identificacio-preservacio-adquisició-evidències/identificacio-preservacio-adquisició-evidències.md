# Identificació, preservació, adquisició d'evidències

**INS Carles Vallbona**

**Pau Tomé**

---
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Identificació, preservació, adquisició d'evidències](#identificaci-preservaci-adquisici-devidncies)
	- [1. IDENTIFICACIO DE L’ESCENARI](#1-identificacio-de-lescenari)
		- [AVALUACIO DE LES EVIDENCIES](#avaluacio-de-les-evidencies)
	- [2. PRESERVACIO](#2-preservacio)
		- [L’ESCENA DE L'INCIDENT O CRIM](#lescena-de-lincident-o-crim)
		- [CADENA DE CUSTODIA](#cadena-de-custodia)
	- [3. ADQUISICIO DE DADES.](#3-adquisicio-de-dades)
		- [ORDRE A SEGUIR EN L'ADQUISICIO D’EVIDENCIES](#ordre-a-seguir-en-ladquisicio-devidencies)
		- [Documentació proposada per l'adquisició](#documentaci-proposada-per-ladquisici)
		- [OFF-LINE VS LIVE EN ADQUISICIO D'EVIDENCIES](#off-line-vs-live-en-adquisicio-devidencies)
			- ["Live” (en calent)](#live-en-calent)
			- [“Offline” (en fred)](#offline-en-fred)
		- [Tipus de còpia](#tipus-de-cpia)
			- [Còpia binària o Bit a bit](#cpia-binria-o-bit-a-bit)
			- [Còpia lògica](#cpia-lgica)
		- [Funcions de hash](#funcions-de-hash)
			- [Utilitat del hashs](#utilitat-del-hashs)
		- [BONES PRACTIQUES D'ADQUISICIO D’EVIDENCIES](#bones-practiques-dadquisicio-devidencies)
		- [REQUISITS DE LES EINES D'ADQUISICIO D’EVIDENCIES](#requisits-de-les-eines-dadquisicio-devidencies)
	- [Normativa](#normativa)
	- [Referències](#referncies)

<!-- /TOC -->
---

<img src="imgs/metodologia-forense-a621b605.png" width="300" align="center" />

Veiem els 3 primers passos de la metodologia forense una mica més en detall.

## 1. IDENTIFICACIO DE L’ESCENARI

**Acotar l’entorn** en el que s’ha produït l'incident. S'ha de recopilar entre d'altres coses:
- Tipus d’evidència involucrada
- Sistemes operatius i programari involucrats
- Format dels sistemes de fitxers
- Localització física de l’evidència (fer fotografies).
- Motiu de la sospita.

Es molt important:
- Tenir professionals amb coneixements adequats
- Disposar d’un **laboratori forense**
- Disposar de **materials apropiats** per recollir i processar les evidències

### AVALUACIO DE LES EVIDENCIES

L’evidència digital ha de ser avaluada respecte l'abast de cada cas:
- Revisar l'**autorització legal**. un exemple por ser l’ordre de registre feta per un jutge.
- Discutir amb l’investigador principal que es pot o no descobrir mitjançant la realització del forense.
- Estudiar la possibilitat d'obtenir altres evidències:
	- P.ex. Enviament d’una **ordre de preservació de dades de tràfic** de xarxa a un ISP
- Considerar la **rellevància dels perifèrics** en el cas:
	- P.ex. Robatori o frau: targetes de crèdit en blanc, paper de xecs, impressores, escàners, etc.
	- Dispositius externs extraïbles, emmagatzemament al núvol, etc.
- Determinar si hi ha **informació addicional** al cas:
	- P.ex. Comptes de correu, ISP, usuaris, configuració de xarxa, etc.

---

## 2. PRESERVACIO

La metodologia de recol·lecció i preservació de les evidències implica **documentar exhaustivament** els següents punts:
- L’escenari
- El mètode d’obtenció de l’evidència
- La cadena de custòdia
- El maquinari i la configuració del sistema
- L’hora i data del sistema
- Les dates i hores clau dels successos (**línia de temps**)

### L’ESCENA DE L'INCIDENT O CRIM

Les següents accions es realitzen habitualment mentre investiguem en l’escena del crim o incident:
- Identificar el **número i tipus d’ordinadors**
- Determinar si hi ha una **xarxa present**
- **Entrevistar** a l‘administrador de sistemes i els usuaris per obtenir informació per exemple de qui pot accedir a què.
- Identificar i documentar els **tipus i volums de dades**, incloent mitjans extraïbles
- Identificar **àrees de emmagatzemament extern**.
- Identificar **software propietari**.
- Etc.

### CADENA DE CUSTODIA

Aquest document assegura la **integritat de l’evidència** com a prova davant processos judicials

**Cada evidència precisa d'un document de cadena de custòdia**. El document de cadena de custòdia **ha d’estar sempre on estigui l’evidència**:
- L'evidència ha d'estar inequívocament identificada (qui, quan, on s'ha recollit)
- Cal informació sobre qui custòdia l'evidència (qui, quan, on, quant de temps, com es guarda)
- Informació sobre cada canvi de custòdia (data, hora, personal involucrat, temps que ha durat, número d'enviament, etc)

---

![](imgs/identificacio-preservacio-adquisició-evidències-8f5bf9f4.png)

Model de document de cadena de custòdia (NIST National Institute of Standards and Technology) - https://www.nist.gov/document/sample-chain-custody-formdocx

---

Explicació sobre el document de custòdia - https://peritoinformatico.es/cadena-de-custodia-peritaje-informatico/

Los peritos confirman la manipulación del disco duro de Déborah Fernández -
https://www.diariodepontevedra.es/articulo/vigo/peritos-confirman-manipulacion-disco-duro-deborah-fernandez/202205262038261202645.html

---

## 3. ADQUISICIO DE DADES.

- Localitzar l’evidència (dispositius a adquirir)
- Assegurar l’escenari
- Descobrir dades rellevants
- Preparar l’**ordre de volatilitat**
- Recollir l’evidència
	- Recuperació d’**informació esborrada** o oculta
	- Duplicat de l’evidència (bit a bit)
- Preparar la **cadena de custòdia**

Directrius per a recuperar evidències (entre elles **RFC 3227**) - https://www.incibe-cert.es/blog/rfc3227

### ORDRE A SEGUIR EN L'ADQUISICIO D’EVIDENCIES

Basat en diferents guies de recol·lecció i arxiu d’evidències electròniques RFC3227 https://datatracker.ietf.org/doc/html/rfc3227 - https://www.incibe-cert.es/blog/rfc3227

**Ordre de volatilitat**: Cal respectar sempre aquest ordre en l'adquisició per evitar que desapareguin les evidències o les contaminem involuntàriament.

1. Registres i memòria cau ("caché")
2. Taula de rutes i memòria cau ARP, memòria cau DNS, connexions de xarxa, taula de processos, estadístiques del Kernel (nucli)
3. Memòria RAM de la màquina
4. Informació temporal del sistema
5. Disc físic
6. Logs del sistema
7. Configuració física i topologia de la xarxa
8. Documents

Alguns **exemples de dades volàtils** importants són:
- Data i hora de l'equip (cal contemplar la zona horària).
- Anàlisi de xarxa (connexions establertes, etc.)
- Bolcat de memòria RAM
- Històric de l'intèrpret de comandes (history en Linux, per exemple)
- Pagefile.sys e hiberfile.sys (Windows)
- Arbre de directoris i fitxers a memòria.
- Historial d'internet (en memòria)
- Carpetes compartides (actualment obertes)
- Usuaris que han iniciat sessió i llistat de comptes
- Processos actius
- Serveis en execució
- Dispositius compartits
- Activitat remota
- Volums xifrats oberts (per exemple Veracrypt).

**La pregunta del milió: S’ha d’apagar una màquina? Doncs depèn del cas. D'entrada, NO.** Abans es tendia a apagar la màquina immediatament i es perdia l'oportunitat d'obtenir dades valuoses en viu. Actualment, s'avalua primer si la màquina s'ha d'apagar.

### Documentació proposada per l'adquisició

Formulari proposat d'adquisició - https://docs.google.com/spreadsheets/d/1VNPwrwg8XsbhAFT9CbtO9kBuBTqpFPv0ugniX9R9Kjo/edit#gid=0

---

### OFF-LINE VS LIVE EN ADQUISICIO D'EVIDENCIES

Bàsicament tenim dues possibilitats per adquirir les evidències:
- "Live” (en calent)
- “Offline” (en fred)

Sempre cal tenir present:
- No sempre és possible recol·lectar dades volàtils. Depèn de les circumstàncies.
- Si es troba un **procés destructiu en marxa**, es recomana aturar-la immediatament.
- Si es troba una connexió remota que posa en marxa el procés destructiu, cal documentar la connexió, tancar-la i adquirir la RAM
- Si l'atacant està connectat remotament accedint a dades, cal pensar si volem mantenir-la mentre capturem la RAM o aturar-la.

#### "Live” (en calent)

Es fa quan no es pot apagar l’equip, per exemple:
- L’equip utilitza **encriptació de disc** i no es te accés a les claus necessàries para desxifrar.
- S’ha de **mantenir el servei** en funcionament per motius de negoci.
- No es vol apagar l’equip per **no modificar el comportament d’un procés maliciós**, etc.

ATENCIÓ: Pot provocar canvis degut a l’ús normal de l'equip (últim accés a arxius, events produïts i entrades de logs noves). Cal documentar el moment en que es comença l'adquisició per poder descartar aquests elements provocats pel procés d'adquisició.

#### “Offline” (en fred)

Es fa amb l'equip apagat. Evita canvis degut a l’ús normal de l'equip (per exemples MAC's d'arxius, events i logs).
- Implica apagar l’equip, treure el disc i col·locar-ho en una estació forense.
- Utilitzar un dispositiu hardware/software per bloquejar les escriptures (**només lectura**) abans de crear la imatge.

---

### Tipus de còpia

Quan fem còpies de dispositius d'emmagatzemament, es poden aplicar dues estratègies:

#### 1. Còpia binària o Bit a bit

Es copien tots els blocs del dispositiu a un fitxer imatge (raw, E01, encase, etc), ignorant el sistema d'arxius d'origen ja que a posteriori es carregarà la imatge per a l'anàlisi amb alguna eina adient. Exemple: còpia amb dd, FTK Imager o d'altres eines d'adquisició forense.

La còpia binària:
- sempre **ocupa molt més i triga molt més** en fer-se
- **permet recuperar dades en zones del disc no assignades**.
- **no es modifiquen metadades** del sistema d'arxius.

**ATENCIÓ**: Es pot fer còpia binària també de dispositiu físic a un dispositiu físic idèntic amb clonadores de discos en maquinari.

#### 2. Còpia lògica

Es copien els fitxers del disc atenent al sistema d'arxius que conté.
- **No es copien les zones no assignades del disc** (no es pot fer "carving" ni buscar a l' "slack space", ni buscar entre particions, ni buscar als sectors marcats com a defectuosos, etc).
- Ex: usar eines com cp, tar, cpio, dump, restore (aquestes eines no es poden usar en forense sense paràmetres que evitin la modificació dels MAC's dels arxius).

**Aplicarem aquesta tècnica quan**:
- No tenim accés al dispositiu físic per extreure'l, per exemple quan treballem al núvol.
- No podem extreure el dispositiu per que està sent utilitzat, per exemple si volem copiar el directori personal d'un sol usuari a un disc de xarxa.
- El temps és limitat.

---

### Funcions de hash

És necessari **calcular sempre el valor de hash de les evidències**.
- El hash és una **funció de xifrat d'un sol sentit** (no és reversible i per tant no es pot recuperar la informació original a partir del hash).
- Per un mètode donat, per exemple MD5, **sempre es genera un hash amb el mateix nombre de bits**, independentment de la mida de les dades d'entrada.
- Ha de ser un **algorisme lliure de col·lisions**: Això vol dir que pràcticament és impossible generar un document que tingui el mateix hash que un altre document donat. Si que pot passar que dos o més documents generin el mateix hash, però no és possible saber quins són.  

Funcions de hash més usades:
- MD5
- Secure Hash Algorithm (SHA) del NIST
  - SHA-1
  - SHA-2
  - SHA-3

**ATENCIÓ**: **MD5 i SHA1 es consideren hash insegurs** (es pot generar un hash concret a partir d'un fitxer preparat), però com que són molt més ràpides el que es fa és calcular totes dues i usar-les en conjunt. Per exemple FTK imager fa servir aquest model.

#### Utilitat del hashs

Principalment el fem servir en tres situacions:
1. **Preservar les evidències**: verificar que l'evidència està intacta i no ha sigut canviada (per errors, etc).
2. **Fer una anàlisi de hash**: comparar el hash de les evidències amb un o més hash de fitxers típics "bons" o "dolents". Per exemple, comparar amb fitxers típics de Windows en un sistema "net" o amb fitxers que contenen malware o són fotografies pornogràfiques.
3. **Verificar positivament que un arxiu ha estat alterat** (per exemple per un malware).

---

### BONES PRACTIQUES D'ADQUISICIO D’EVIDENCIES

L’adquisició de l‘evidència digital s’ha de produir de tal manera que aquesta sigui **preservada**:
1. Documentar el **hardware/software (comandes) utilitzades**
2. Obrir l’ordinador per **tenir accés físic als discs**
3. **Protegir-los** d'electricitat estàtica i camps magnètics (bosses antiestàtiques i caixes de Faraday).
4. Documentar aquesta acció i realitzar-la **davant testimonis** (si pot ser un notari, millor).
5. **Identificar els dispositius d'emmagatzematge** (interns o externs) que es necessari adquirir
6. Documentar els **dispositius d'emmagatzematge interns i la configuració hardware** del equip:
	- Estat del disc (marca, model, geometria, interfície, etc.)
	- Components interns (targetes de so, gràfiques, de xarxa, etc.)
9. **Comprovar si el disc està encriptat** abans d’apagar l’equip (es recomana adquirir en calent)
10. **Desconnectar els dispositius d'emmagatzematge** per prevenir la destrucció o alteració de les dades
11. Realitzar l’adquisició utilitzant l’**equip de l’examinador**
	(A excepció de RAID, portàtils, hardware propietari, etc.)
13. És aconsellable l’**ús de dispositius de protecció d’escriptura** per evitar modificar el disc original
14. El **disc destí ha d'estar “net”** en termes forenses:
  - Disc nou recent estrenat
  - Disc utilitzat formatat indicant el procediment a l'informe (wipe)
17. És recomanable crear un valor conegut de l'evidència original abans d’adquirir-la (**Càlcul d’un hash** del disc, p.ex. SHA1)
19. Adquirir l’evidència utilitzant software o hardware testejat i verificar l’adquisició
	- Realitzar-la **bit a bit** per que contingui els fitxers esborrats (per a fer "carving") i els "file slack" (espai del clúster no aprofitat)
	- **Comparació del hash** original respecte el de la còpia
22. **Xifrar les imatges forenses** para garantir la confidencialitat i establir la cadena de custòdia correcta per la imatge
23. Investigar **sempre sobre les còpies**, mai sobre l'original.

---

### REQUISITS DE LES EINES D'ADQUISICIO D’EVIDENCIES

Necessitem eines molt variades, però en general **cal que compleixin**:
- Els programes han de estar **compilats amb enllaços de llibreries (biblioteques, millor dit) estàtiques** ("statically linked"), o sigui, no han de fer servir llibreries compartides (DLL). Les que facin servir han d'estar al dispositiu de només lectura.
- Tot i això, els rootkits poden ser instal·lats via mòduls del Kernel i per tant pot ser que no es vegin a la captura de memòria.
- Cal poder **demostrar que les eines són confiables i lícites**. Existeix un projecte per sistematitzar les proves del programari forense al NIST (National Institute of Standards and Technology) dels EUA.

Més informació: Computer Forensics Tool Testing Program (CFTT) (https://www.nist.gov/itl/ssd/software-quality-group/computer-forensics-tool-testing-program-cftt).

Hi ha una llista d'eines testades pel NIST aquí: https://www.nist.gov/itl/ssd/software-quality-group/computer-forensics-tool-testing-program-cftt/cftt-technical

---

#### Algunes eines que necessitem

**Eines de preservació d’evidències**
- Bloqueig lògic d’escriptura en els discs originals
- Suport de formats RAW, Encase EWF, AFF, etc.
- Traçabilitat (cadena de custòdia)
- Càlcul de hashs criptogràfics

**Eines de reducció i selecció de dades ("triage") ràpida**
- Detecció de firmes de fitxers
- Filtres avançats i motor de cerca

**Eines de reconstrucció de volums i sistemes de fitxers**
- Detecció i muntatge de particions
- Suport de format VMDK (Virtual Machine Disk)
- Suport de FAT12/16/32 (USB)
- Suport de NTFS amb ADS https://es.wikipedia.org/wiki/Alternate_Data_Streams i compressió (Windows)
- Suport de HFS, HFS+ y HFSX (OS X, iPhone)
- Suport de Ext2/3/4 (GNU Linux, Android)

**Eines d'anàlisi multimèdia**
- Visualització de galeries de fotografies
- Extracció de miniatures de vídeos
- Extracció de metadades EXIF (Exchangeable image file format)

**Eines d'anàlisi d’artefactes Windows**
- Fitxers LNK (Accès directe)
- Fitxers Prefetch (precàrrega d’aplicacions)
- Registre de Windows
- Bústies PST (Outlook)

**Eines d'anàlisi de memòria**
- Integració amb Volatility Framework
- Reconstrucció gràfica de processos: pstree, psxview
- Informació de processos: procdump

**Eines d'anàlisi de documents**
- Visualitzadors dedicats: PDF, Text, Web, etc.
- Extracció de metadades, text i imatges en documents ofimàtics.


---
## Normativa

- **RFC 3227** Directrius per a recuperar evidències - https://www.certsi.es/blog/rfc3227

- **ISO/IEC 27037:2012** Normativa para la identificación, recolección, adquisición y conservación, de evidencias digitales - https://peritosinformaticos.es/iso-iec-270372012-perito-informatico/
- **ISO/IEC 27042:2015**. Normativa para el análisis e interpretación de evidencias digitales - https://peritosinformaticos.es/iso-27042-perito-informatico/
- **UNE 71505/2013**. Sistema de Gestión de Evidencias Electrónicas - https://peritosinformaticos.es/une-71505/
  - UNE 71505/2013-1. Sistema de Gestión de Evidencias Electrónicas Parte 1: Vocabulario y principios generales. - https://peritosinformaticos.es/une-71505-perito-informatico-3/
  - UNE 71505/2013-2. Buenas prácticas en la gestión de las evidencias electrónicas - https://peritosinformaticos.es/une-71505-perito-informatico/
  - UNE 71505/2013-3. Formatos y mecanismos técnicos - https://peritosinformaticos.es/une-71505-perito-informatico-2/

- **UNE 71506/2013**. Metodología para el análisis forense de las evidencias electrónicas. - https://peritosinformaticos.es/une-71506-perito-informatico/

- **UNE 197010/2015**. Normas Generales para la elaboración de informes y dictámenes periciales sobre TIC. - https://peritosinformaticos.es/une-197010-perito-informatico/

---

## Referències

- Què és una imatge forense - https://www.raedts.biz/forensics/forensics-101-forensic-image/
