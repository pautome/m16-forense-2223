# Volatility

**INS Carles Vallbona**

**Pau Tomé**

---
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Volatility](#volatility)
	- [Què és Volatility?](#qu-s-volatility)
	- [Entorn GUI](#entorn-gui)
	- [Pas 1: Trobar perfil i executar Volatility (Volatility 2)](#pas-1-trobar-perfil-i-executar-volatility-volatility-2)
	- [Pas 2: Usar els Plugins](#pas-2-usar-els-plugins)
		- [Processos i DLLs](#processos-i-dlls)
		- [Comandes, portaretalls, variables d'entorn](#comandes-portaretalls-variables-dentorn)
		- [Fitxers a memòria (si n'hi ha)](#fitxers-a-memria-si-nhi-ha)
		- [Extreure memòria de processos](#extreure-memria-de-processos)
		- [Xarxa](#xarxa)
		- [Malware](#malware)
		- [Hashes i passwords](#hashes-i-passwords)
		- [Altres](#altres)
		- [Greps Interessants](#greps-interessants)
		- [Plugins externs](#plugins-externs)
	- [Referències](#referncies)

<!-- /TOC -->
---

## Què és Volatility?

Volatility és una eina forense de codi obert per a la resposta a incidents i l'anàlisi de malware. És capaç d'instal·lar-se en qualsevol sistema sobre el qual es disposi de Python, per la qual cosa és compatible amb multitud de sistemes (Windows, Linux o Mac), la qual cosa facilita molt l'ús del mateix en diferents sistemes operatius.

Disposa a més d'una API extensible i programable, on a més dels propis mòduls que ofereix el propi Volatility és possible la creació de mòduls personalitzats amb la finalitat de dur a terme anàlisis d'una forma automàtica, la creació d'una interfície web o GUI personalitzada o simplement explorar la memòria del kernel d'una forma automatitzada. La documentació pròpia és molt extensa i es troba a la pàgina oficial.

Volatility proporciona capacitats com podria ser fer un carving d'un històric de comandes, buffer d'entrada/sortida de la consola, objectes d'usuari i estructures de dades relacionades amb la xarxa.

Permet formats d'arxiu diferents com ara bolcats sense processar, de caiguda del sistema, arxius d'hibernació, estats guardats de equips virtuals amb VMware o bolcats del nucli de VirtualBox, LiME (Linux Memory Extractor), expert witness (EWF) i memòria física directa sobre Firewire. A més també és possible convertir entre els diferents formats disponibles per si utilitzem una eina diferent d'aquesta i necessitem un format concret.

Disposa a més d'una sèrie d'algorismes ràpids i eficients que li permeten analitzar els bolcats de memòria RAM de sistemes de gran volum sense un consum de recursos excessiu i en molt poc temps.

## Entorn GUI

Es disposa d'un GUI sense suport oficial per Volatility

https://github.com/AdityaSec/Vol-GUI

## Instal·lació (versió 2)

Per instal·lar i executar podem fer:
~~~
sudo apt install volatility  
o  
git clone https://github.com/volatilityfoundation/volatility
cd volatility
python vol.py
~~~

**NOTA: Podem executar la comanda "python vol.py" o bé "volatility" segons el sistema.**

## Instal·lar versió 3

- https://github.com/volatilityfoundation/volatility3

~~~
// Instal·lar Python3
sudo apt install python3 python3-pip

git clone https://github.com/volatilityfoundation/volatility3.git
cd volatility3
pip3 install wheel

// Instal·la tots els paquets extra per treballar amb tot
pip3 install -r requirements.txt

// Descarregar taules de símbols Windows al dir de simbols
cd volatility3/symbols
wget https://downloads.volatilityfoundation.org/volatility3/symbols/windows.zip

// Executar plugin pslist. La primera vegada ha de cobstruir la taula de símbols i estarà una estona
python3 vol.py -f samples/cridex.vmem windows.pslist
~~~

---

## Plugins recomanats

### Distorm3

Llibreria Desensamblador per x86/AMD64. Necessari si volem fer servir els plugins (comandes)
- apihooks
- callbacks
- impscan
- kdbgscan, pslist, modules etc for Windows 8/2012 machines
- les comanda volshell, linux_volshell i mac_volshell

~~~
pip install distorm3
~~~

### Yara

Detector i classificador de malware. Necessari pels plugins
- yarascan, linux_yarascan, mac_yarascan

~~~
pip install yara
~~~

### PyCrypto

Toolkit de criptografia de Python. Necessari pels plugins
- lsadump
- hashdump

~~~
pip install pycrypto
~~~

### PIL - Python Imaging Library
Necessari pel plugin "screenshots"

~~~
pip install pillow
~~~

---

## Pas 1: Trobar perfil i executar Volatility (Volatility 2)

Trobar el profile (perfil) que funciona amb el fitxer imatge que volem analitzar - **imageinfo**
~~~
volatility -f ruta_fitxer_imatge imageinfo
~~~

Provem els "profiles" amb la comanda **pslist** de Volatility. El profile que retorni més resultats (número de processos) sol ser el correcte. Per exemple:
~~~
volatility -f MEMORY_FILE.raw --profile=WinXPSP2x86 pslist
~~~

També podem usar la comanda kdbgscan. Si veiem una sortida com ara aquesta on el nombre de processos i mòduls és 0, no és el perfil correcte.
~~~
volatility -f Win2K3SP2x64-6f1bedec.vmem --profile=Win2003SP2x64 kdbgscan
Volatility Foundation Volatility Framework 2.4
**************************************************
Instantiating KDBG using: Kernel AS Win2003SP2x64 (5.2.3791 64bit)
Offset (V)                    : 0xf80001172cb0
Offset (P)                    : 0x1172cb0
KDBG owner tag check          : True
Profile suggestion (KDBGHeader): Win2003SP2x64
Version64                     : 0xf80001172c70 (Major: 15, Minor: 3790)
Service Pack (CmNtCSDVersion) : 0
Build string (NtBuildLab)     : T?
PsActiveProcessHead           : 0xfffff800011947f0 (0 processes)
PsLoadedModuleList            : 0xfffff80001197ac0 (0 modules)
KernelBase                    : 0xfffff80001000000 (Matches MZ: True)
Major (OptionalHeader)        : 5
Minor (OptionalHeader)        : 2
~~~

Un cop trobat el perfil, per executar Volatility (la comanda depén del plugin que ha d'estar instal·lat):  
~~~
volatility -f ruta_fitxer_imatge --profile=Nom_Profile  Nom_comanda
~~~

---

## Pas 2: Usar els Plugins

### Processos i DLLs  

1. Per llistar els **processos a memòria** - **pslist**
~~~
volatility -f ruta_fitxer_imatge --profile=Win7SP1x64 pslist
~~~

2. Per mostrar l'arbre de processos, veure processos que han generat a d'altres processos, la qual cosa és important per seguir la pista de quin és el programa que ha creat el procés del malware - **pstree**
~~~
volatility -f ruta_fitxer_imatge --profile=Win7SP1x64 pstree
~~~

3. Per mostrar **processos ocults**, de manera que els que tenen "False" a alguna columna són susceptibles de ser malware. Podem filtrar les línies amb "grep False"  - **psxview**
~~~
volatility -f ruta_fitxer_imatge --profile=Win7SP1x64 psxview
~~~

A més de veure els processos ocults a través de **psxview**, també podem comprovar-ho amb un enfocament més ampli a través de l'ordre **ldrmodules**. Apareixeran al llistat tres columnes al mig, InLoad, InInit, InMem. Si algun dels valors d'aquestes columnes és "fals", probablement s'ha injectat aquest mòdul, indicant possible malware. En un sistema normal, l'expressió "grep False" anterior no hauria de retornar cap sortida.

4. Per mostrar les DLL que ha carregat un determinat procés o tots els processos si no indiquem cap. Les DLLs són biblioteques de sistema compartides utilitzades en processos de sistema. Aquests són sotmesos habitualment a atacs de segrest i altres atacs de càrrega lateral - **dllist** -p pid
~~~
python vol.py -f ~/Desktop/win7_trial_64bit.raw --profile=Win7SP0x64 dlllist -p 1892
~~~

5. Per veure **patches inesperats en el sistema estàndard de DLLs**. Si veiem una instància on el mòdul tingui "Hooking: unknown" tenim indicis de malware. - **apihooks**
~~~
volatility -f MEMORY_FILE.raw --profile=PROFILE apihooks
~~~

6. Per **extreure les DLLs i poder-les examinar**, per exemple enviant la mostra a Virustotal (si no indiquem pid, ho fa de tots els processos) - **dlldump**

~~~
volatility -f MEMORY_FILE.raw --profile=PROFILE --pid=PID dlldump -D directori_destí
~~~

---

### Comandes, portaretalls, variables d'entorn

1. Per llistar les **comandes executades** - **cmdscan**
~~~
volatility -f ruta_fitxer_imatge --profile=Win7SP1x64 cmdscan
~~~

2. Per veure la **sortida de les comandes executades** - **consoles**
~~~
volatility -f ruta_fitxer_imatge --profile=Win7SP1x64 consoles
~~~

3. Per veure el **contingut del portaretalls** - **clipboard**
~~~
volatility -f ruta_fitxer_imatge --profile=Win7SP1x64 clipboard
~~~

4. Per veure les **variables d'entorn** - **envars**
~~~
volatility -f ruta_fitxer_imatge --profile=Win7SP1x64 envars
~~~

---

### Fitxers a memòria (si n'hi ha)

1. **Escannejar fitxers** - **filescan**

(Caldria aplicar filtres de l'estil "grep" amb carpetes coma ara  Documents/Downloads/Desktop i filtrar amb grep paraules coma ara  .kdbx, .rar, .zip, .png, .bmp, .jpg/jpeg, important, Pass/Password)
~~~
volatility -f ruta_fitxer_imatge --profile=Win7SP1x64 filescan | grep Documents
~~~

2. **Extreure fitxers** (abans haurem scannejat per saber la localització hexadecimal del fitxer que volem extreure) - **dumpfiles** -Q localització-Hexadecimal -D ruta_destí
~~~
volatility -f ruta_fitxer_imatge --profile=Win7SP1x64  -Q 0x0000000017663e7 -D .
~~~

3. Per **veure arxius esborrats, fitxers modificats i data de creació de la còpia de la MFT** que hi ha a memòria - **mftparser**
~~~
volatility -f ruta_fitxer_imatge --profile=Win7SP1x64 mftparser
~~~

---

### Extreure memòria de processos

1. **Extreure memòria resident de processos** en particular - **memdump** -p pid -D ruta_destí
~~~
volatility -f ruta_fitxer_imatge --profile=Win7SP1x64 memdump -P 231 -D .
~~~

2. Extreure el programa executable de processos en particular - **procdump** -p pid -D ruta_destí
~~~
python vol.py -f win7_trial_64bit.raw --profile=Win7SP0x64 procdump -p 296 -D dades/
~~~


3. Per veure les **comandes de determinats processos** - **cmdline** -p pid1, pid2 ...
~~~
volatility -f ruta_fitxer_imatge --profile=Win7SP1x64 -P 123,234
~~~

---

### Xarxa

1. Per veure les **connexions de xarxa** (Windows Vista, Windows 2008 Server i Windows 7) - **netscan**
~~~
python vol.py --plugins=../volatility/plugins --cache -f victim.raw --profile=Win7SP1x64 netscan
~~~

2. Per veure les **connexions de xarxa** (Windows XP i Windows 2003 Server) - **connscan**

---

### Malware

1. Per buscar **processos que tenen pàgines amb protecció PAGE_EXECUTE_READWRITE** que pot ser indicador de malware - **malfind**

~~~
python vol.py --plugins=../volatility/plugins --cache -f victim.raw --profile=Win7SP1x64 malfind
~~~

---

### Hashes i passwords

1. Per **extreure els hashes de passwords del sistema**, que després es poden intentar trencar amb eines com hashcat o John the Ripper o inclús eines web - **hashdump**
~~~
python vol.py -f Snapshot6.vmem --profile=Win7SP1x64 hashdump
~~~

2. Per obtenir la **Passphrase de truecrypt** si es troba a memòria - **truecryptpassphrase**
~~~
python vol.py -f part3.vmem --profile=Win7SP1x64 truecryptpassphrase
~~~

---

### Altres

1. Per veure l'**última hora de tancament del sistema** - **shutdowntime**
~~~
volatility -f ruta_fitxer_imatge --profile=Win7SP1x64 shutdowntime
~~~

2. Per extreure **pantalles de les aplicacions en vista de "filferros"** - **screenshot** -D ruta_destí
~~~
volatility -f ruta_fitxer_imatge --profile=Win7SP1x64 screenshot -D .
~~~

---

### Greps Interessants      
~~~
strings Challenge.raw | grep "Mega"
strings Challenge.raw | grep "Pastebin"
strings Challenge.raw | grep "Passwords"
strings Challenge.raw | grep "Flag{"
~~~

### Plugins externs

Per usar i instal·lar plugins extra com ara historial de Chrome o de Firefox es pot clonar el repo github: https://github.com/superponible/volatility-plugins

---

## Referències

- Generar un bolcat de memòria de Windows -
  Want to crash a system to generate a memory dump? Open PowerShell as an admin and run winnit.exe - No need to use additional tools.

- Anàlisi de bolcats de memòria amb Volatility - https://www.cybervie.com/blog/analyzing-memory-dumps-with-volatility/

- Convertir bolcats de memòria per usar a Volatility - https://github.com/volatilityfoundation/volatility/wiki/Command-Reference#crash-dumps-hibernation-and-conversion

- Crash dump de Windows en Autopsy - https://github.com/volatilityfoundation/volatility/wiki/Crash-Address-Space

- Generar crash dumps (Microsoft) - https://docs.microsoft.com/es-ES/windows/client-management/generate-kernel-or-complete-crash-dump
